import { shallow } from 'enzyme';
import React from 'react';
import sampleArticles from '../apiSample_10Articles';
import Article from './Article';

describe('Article render', () => {

  const testArticle = sampleArticles[0];
  let articleWrapper;

  beforeAll(() => {
    articleWrapper = shallow(<Article data={testArticle}/>);
  });

  it('should render correctly with valid data', () => {
    expect(articleWrapper.getElement()).toMatchSnapshot();
  });

  it('should provide a link to the full article', () => {
    expect(articleWrapper.find('a').getElement().props.href)
      .toEqual(testArticle.webUrl);
  });
});

