import { Box } from 'grommet';
import React from 'react';
import Article from './Article.js';

export default props => (
  <Box align={'start'} {...props}>
    {props.articles.map(a => <Article key={a.id} data={a}/>)}
  </Box>);
