import { shallow } from 'enzyme';
import { TextInput } from 'grommet';
import React from 'react';
import SearchField from './SearchField.js';

describe('Search field', () => {

  it('should use provided change handler', () => {
    const fakeArticlesProvider = jest.fn();
    const searchFieldWrapper = shallow(<SearchField
      onChange={fakeArticlesProvider}/>);
    const testQuery = 'foo';
    const fakeEvent = {target: {value: testQuery}};
    searchFieldWrapper.find(TextInput).simulate('change', fakeEvent);
    expect(fakeArticlesProvider).toHaveBeenCalledWith(fakeEvent);
  });

  it('should render as expected', () => {
    expect(shallow(<SearchField/>).getElement()).toMatchSnapshot();
  });
});
