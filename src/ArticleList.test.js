import { shallow } from 'enzyme';
import React from 'react';
import sampleArticles from '../apiSample_10Articles';
import ArticleList from './ArticleList.js';

describe('ArticleList render', () => {

  it('should render as expected with no articles', () => {
    expect(shallow(<ArticleList articles={[]}/>).getElement())
      .toMatchSnapshot();
  });

  it('should render as expected with articles', () => {
    expect(shallow(<ArticleList articles={sampleArticles}/>).getElement())
      .toMatchSnapshot();
  });
});
